import { Routes, Route  } from 'react-router-dom'

import Main from 'pages/Main'
import SignIn from 'pages/User/SignIn'
import SignUp from 'pages/User/SignUp'

function AppRoutes() {
    return (
        <Routes>
            <Route path="/" element={ <Main /> }>
                <Route path="user/sign-in" element={ <SignIn /> } />
                <Route path="user/sign-up" element={ <SignUp /> } />
                <Route path="user/me" element={ <p>Me</p> } />
            </Route>
        </Routes>
    )
}

export default AppRoutes
