<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'App') }}</title>

    <script id="user-data" type="application/json">
        {
            "login":"tyomka896",
            "first_name":"Artyom",
            "last_name":"Pronin",
            "email":"artempronin96list.ru",
            "_token":"T1q7SBQCOLCyVlpc"
        }
    </script>
</head>
<body>
    <div id="root"></div>
    @viteReactRefresh
    @vite('resources/src/index.jsx', 'src')
</body>
</html>
