import { defineConfig } from 'vite'
import laravel from 'laravel-vite-plugin'
import react from '@vitejs/plugin-react'

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/src/index.jsx'],
            refresh: true,
        }),
        react(),
    ],
    build: {
        outDir: 'public/src',
        rollupOptions: {
            output: {
                entryFileNames: '[name]-[hash].js',
                chunkFileNames: 'chunks/[name]-[hash].js',
                assetFileNames: ({ name }) => {
                    if (/\.(gif|jpe?g|png|svg)$/.test(name ?? ''))
                        return 'images/[name]-[hash][extname]'
                    if (/\.css$/.test(name ?? ''))
                        return '[name]-[hash][extname]'
                    return 'assets/[name]-[hash][extname]'
                },
            }
        }
    },
    define: { 'process.env': process.env },
    resolve: {
        alias: [
            { find: 'pages', replacement: 'resources/src/pages' },
            { find: 'styles', replacement: 'resources/src/styles' },
            { find: 'themes', replacement: 'resources/src/themes' },
            { find: 'utils', replacement: 'resources/src/utils' },
        ]
    },
})
