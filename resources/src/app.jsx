import { BrowserRouter  } from 'react-router-dom'

import auth from 'utils/auth'
// console.log('$ auth', auth.login)

import AppRoutes from './routes'

function App() {
    return (
        <BrowserRouter>
            <AppRoutes />
        </BrowserRouter>
    )
}

export default App
