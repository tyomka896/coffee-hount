<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{path?}', function () {
    return view('index');
})->where('path', '^(?!api\/).*$');


/*
|--------------------------------------------------------------------------
| User authentication
|--------------------------------------------------------------------------
*/

Route::middleware('guest')->group(function () {
    Route::get('register', 'RegisteredUserController@create');
    Route::post('register', 'RegisteredUserController@store');
    Route::get('login', 'AuthenticatedSessionController@create');
    Route::post('login', 'AuthenticatedSessionController@store');
});

Route::middleware('auth')->group(function () {
    Route::post('logout', 'AuthenticatedSessionController@destroy');
});
