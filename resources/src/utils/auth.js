
const auth = JSON.parse(document.getElementById('user-data').textContent)

export let APP_NAME = process.env.MIX_APP_NAME || 'COFFEE'

export let TOKEN = document.querySelector('meta[name=csrf-token]').getAttribute('content')

export default auth
