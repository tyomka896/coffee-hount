import { Outlet  } from 'react-router-dom'

import Navbar from 'themes/Navbar'

function MainPage() {
    return (
        <>
            <div className="navbar">
                <Navbar />
            </div>
            <div className="container">
                <Outlet />
            </div>
        </>
    )
}

export default MainPage
